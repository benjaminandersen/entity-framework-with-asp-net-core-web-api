# movieCharacterAPI

## Name
Entity Framework with ASP.NET Web API

## Description
Entity Framework Code First workflow and an ASP.NET Core Web API in C#. Entity Framework Code First workflow was used to create the database through a DbContext adding migrations.
API is able to call 20 various requests to read, edit, update and delete items in the created database.

## Installation
Install AutoMapper

``````bash
dotnet add [PROJECT NAME] package AutoMapper --version 10.1.1
``````

Install AutoMapper.Extensions.Microsoft.DependencyInjection

``````bash
dotnet add [PROJECT NAME] package AutoMapper.Extensions.Microsoft.DependencyInjection --version 8.1.1 
``````

Install Microsoft.EntityFrameworkCore

``````bash
dotnet add [PROJECT NAME] package Microsoft.EntityFrameworkCore --version 5.0.9
``````

Install Microsoft.EntityFrameworkCore.SqlServer

``````bash
dotnet add [PROJECT NAME] package EntityFrameworkCore.SqlServer --version 5.0.9
``````

Install Microsoft.EntityFrameworkCore.Tools

``````bash
dotnet add [PROJECT NAME] package Microsoft.EntityFrameworkCore.Tools --version 5.0.9
``````

Install Swashbuckle.AspNetCore

``````bash
dotnet add [PROJECT NAME] package Swashbuckle.AspNetCore --version 6.2.1
``````

## Usage
For information about usage, look at the implemented summary tags.

Following comes a guided description to run the API and tests its requests.

### Before running the application
Navigate to appsettings.json and 'DefaultConncetion': switch out 'INSERT SERVER NAME' with your specific server name.

### Building database
Navigate to Package Manager Console: 
	
1. Type 'add-migration' followed by migration name in console to initiate migrations.

``````bash
add-migration 'migrationName'
``````

2. Type 'update-database' to run migrations.

``````bash
update-database
``````

### Run application
Run application by pressing IIS Express where the green arrow is displayed. 

### Running API requests on database
Following comes a walkthrough of how to initiate the various requests available:

#### Characters

	1. Get All Characters
		- Open 'Get all characters' --> Try it out --> Execute
		- This will display a list of all characters, including all related metadata.

	2. Add New Character
		- Open 'Add a new character to the database' --> Try it out --> Fill in body with desired character values ('movie' cannot be NULL) --> Execute
		- This will display the newly added character. Character ID will be autoincremented.

	3. Get Specific Character
		- Open 'Get a specific character by ID' --> Try it out --> Fill in desired ID --> Execute
		- This will display the specific character with ID input and all related metadata.

	4. Update Specific Character
		- Open 'Update a specific character by character ID' --> Try it out --> Fill in specific character ID to update desired character 
			--> Fill in body with with desired character values ('movie' cannot be NULL, 'id' must match input ID) --> Execute
		- This will display the updated character.

	5. Delete Specific Character
		- Open 'Delete a character from the database' --> Try it out --> Fill in specific character ID to delete desired character
		- This will delete the character with the corresponding input ID. The GetAllCharacters request can be run again to verify that the character is no longer in the character list.

#### Franchises

	1. Get All Franchises
		- Open 'Get all franchises' --> Try it out --> Execute
		- This will display a list of all franchises, including all related metadata.

	2. Add New Franchise
		- Open 'Add a new franchise to the database' --> Try it out --> Fill in body with desired franchise values --> Execute
		- This will display the newly added franchise. Franchise ID will be autoincremented.

	3. Get Specific Franchise
		- Open 'Get a specific franchise by ID' --> Try it out --> Fill in desired ID (must be an existing franchise ID) --> Execute
		- This will display the specific franchise with ID input and all related metadata.

	4. Update Specific Franchise
		- Open 'Update a specific franchise by ID' --> Try it out --> Fill in specific franchise ID to update desired franchise
			--> Fill in body with with desired franchise values ('id' must match input ID) --> Execute
		- This will display the updated franchise.

	5. Delete Specific Franchise
		- Open 'Delete a franchise by specific ID' --> Try it out --> Fill in specific franchise ID to delete desired franchise 
			(if franchise has movies, the 'Update Movies in Franchise' request must first be run to empty the movie list, in order to delete franchise)
		- This will delete the franchise with the corresponding input ID. The GetAllFranchises request can be run again to verify that the franchise is no longer in the franchise list.

	6. Get All Movies in Franchise
		- Open 'Get a list of all the movies in a specific franchise by ID' --> Try it out --> Fill in desired franchise ID
		- This will display the movies in that specific franchise with related metadata.

	7. Update Movies in Franchise
		- Open 'Update a franchise's movies by franchise ID' --> Try it out --> Fill in desired franchise ID
			--> Fill in body with movie ID(s) to add to the specific franchise (must fill in all movies in franchise, including movies already in franchise - if not, they will be removed)
		- This will display a list with the franchise's updated movies.

	8. Get Characters in Franchise
		- Open 'Get a list of all the characters involved in a specific franchise by ID' --> Try it out --> Fill in desired franchise ID
		- This will display the characters in that specific franchise with related metadata.

#### Movies

	1. Get All Movies
		- Open 'Get all movies' --> Try it out --> Execute
		- This will display a list of all movies, including all related metadata. 

	2. Add New Movie
		- Open 'Add a new movie to the database' --> Try it out --> Fill in body with desired movie values ('franchiseId' must correspond to a valid franchise) --> Execute
		- This will display the newly added movie. Movie ID will be autoincremented.

	3. Get Specific Movie
		- Open 'Get a specific movie by ID' --> Try it out --> Fill in desired ID (must be an existing movie ID) --> Execute
		- This will display the specific movie with ID input and all related metadata.

	4. Update Specific Movie
		- Open 'Update a specific movie by ID' --> Try it out --> Fill in specific movie ID to update desired movie
			--> Fill in body with with desired movie values ('id' must match input ID, 'franchiseId' must correspond to a valid franchise) --> Execute
		- This will display the updated movie.

	5. Delete Specific Movie
		- Open 'Delete a movie from the database' --> Try it out --> Fill in specific movie ID to delete desired movie
		- This will delete the movie with the corresponding input ID. The GetAllMovies request can be run again to verify that the movie is no longer in the movie list.

	6. Get Characters in Movie
		- Open 'Get all characters in a movie' --> Try it out --> Fill in desired movie ID
		- This will display the characters in that specific movie with related metadata.

	7. Update Characters in Movie
		- Open 'Update a movie's characters by movie ID' --> Try it out --> Fill in desired movie ID
			--> Fill in body with character ID(s) to add to the specific movie (must fill in all characters in movie, including characters already in movie - if not, they will be removed)
		- This will display a list with the movie's updated characters.