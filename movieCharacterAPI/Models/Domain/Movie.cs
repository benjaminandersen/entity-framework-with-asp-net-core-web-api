﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace movieCharacterAPI.Models.Domain
{
    [Table("Movie")]
    public class Movie
    {
        // Primary Key
        public int Id { get; set; }

        // Fields
        [Required]
        [MaxLength(150)]
        public string Title { get; set; }

        public string Genre { get; set; }

        public int ReleaseYear { get; set; }

        [MaxLength(50)]
        public string DirectorName { get; set; }

        public string Picture { get; set; }

        public string Trailer { get; set; }

        // Relationships
        public ICollection<Character> Characters { get; set; }
        public int? FranchiseId { get; set; }
        public Franchise Franchise { get; set; }
    }
}