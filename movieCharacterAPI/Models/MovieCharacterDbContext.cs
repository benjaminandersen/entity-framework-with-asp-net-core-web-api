﻿using Microsoft.EntityFrameworkCore;
using movieCharacterAPI.Models.Domain;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading.Tasks;

namespace movieCharacterAPI
{
    public class MovieCharacterDbContext : DbContext
    {
        // Tables
        public DbSet<Character> Characters { get; set; }
        public DbSet<Movie> Movies { get; set; }
        public DbSet<Franchise> Franchises { get; set; }

        public MovieCharacterDbContext([NotNullAttribute] DbContextOptions options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // Seed data
            modelBuilder.Entity<Movie>().HasData(new Movie
            {
                Id = 1,
                Title = "The Lord of the Rings: The Fellowship of the Ring",
                Genre = "Drama, Fantasy, Epic, Adventure, Action",
                ReleaseYear = 2001,
                DirectorName = "Peter Jackson",
                Picture = "https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcT9J7XACn3tlD6v4UXRMvT2wJN8FGCCPeh8U3RkZ6__tR4wGhSo",
                Trailer = "https://www.youtube.com/watch?v=V75dMMIW2B4",
                FranchiseId = 1
            });

            modelBuilder.Entity<Movie>().HasData(new Movie
            {
                Id = 2,
                Title = "The Lord of the Rings: The Return of the King",
                Genre = "Drama, Fantasy, Epic, Adventure, Action",
                ReleaseYear = 2003,
                DirectorName = "Peter Jackson",
                Picture = "https://m.media-amazon.com/images/M/MV5BNzA5ZDNlZWMtM2NhNS00NDJjLTk4NDItYTRmY2EwMWZlMTY3XkEyXkFqcGdeQXVyNzkwMjQ5NzM@._V1_.jpg",
                Trailer = "https://www.youtube.com/results?search_query=lord+of+the+rings+return+of+the+king+trailer",
                FranchiseId = 1
            });

            modelBuilder.Entity<Movie>().HasData(new Movie
            {
                Id = 3,
                Title = "Star Wars Episode IV: A New Hope",
                Genre = "Science Fiction, Action, Space Opera, Epic, Adventure, Fantasy, Mystery",
                ReleaseYear = 1977,
                DirectorName = "George Lucas",
                Picture = "https://m.media-amazon.com/images/M/MV5BNzVlY2MwMjktM2E4OS00Y2Y3LWE3ZjctYzhkZGM3YzA1ZWM2XkEyXkFqcGdeQXVyNzkwMjQ5NzM@._V1_.jpg",
                Trailer = "https://www.youtube.com/watch?v=1g3_CFmnU7k",
                FranchiseId = 2
            });


            modelBuilder.Entity<Character>().HasData(new Character
            {
                Id = 1,
                Name = "Peregrin Tók",
                Alias = "Fool of a Tók",
                Gender = "Male",
                Picture = "https://static.wikia.nocookie.net/pjhobbitlotr/images/8/86/PippinBoyd.png/revision/latest?cb=20170219174505",
            });

            modelBuilder.Entity<Character>().HasData(new Character
            {
                Id = 2,
                Name = "Meriadoc Brandybuck",
                Alias = "Merry",
                Gender = "Male",
                Picture = "https://www.looper.com/img/gallery/amazons-lord-of-the-rings-series-dominic-monaghan-would-star-under-one-condition/intro-1529082447.jpg",
            });

            modelBuilder.Entity<Character>().HasData(new Character
            {
                Id = 3,
                Name = "Stormtrooper with Binoculars",
                Alias = "",
                Gender = "Unknown",
                Picture = "http://swex.pl/wp-content/uploads/2017/05/stormtrooper-binoculars.jpg"
            });


            modelBuilder.Entity<Franchise>().HasData(new Franchise
            {
                Id = 1,
                Name = "The Lord of the Rings",
                Description = "The Lord of the Rings is a series of three epic fantasy adventure films directed by Peter Jackson, based on the novel written by J. R. R. Tolkien. The films are subtitled The Fellowship of the Ring, The Two Towers, and The Return of the King."
            });

            modelBuilder.Entity<Franchise>().HasData(new Franchise
            {
                Id = 2,
                Name = "Star Wars",
                Description = "Star Wars is an American epic space opera multimedia franchise created by George Lucas, which began with the eponymous 1977 film and quickly became a worldwide pop-culture phenomenon."
            });

            // Seed many2many character-movie -> defined below through linking table
            modelBuilder.Entity<Character>()
                .HasMany(p => p.Movies)
                .WithMany(m => m.Characters)
                .UsingEntity<Dictionary<string, object>>(
                "CharacterMovie",
                r => r.HasOne<Movie>().WithMany().HasForeignKey("MovieId"),
                l => l.HasOne<Character>().WithMany().HasForeignKey("CharacterId"),
                je =>
                {
                    je.HasKey("CharacterId", "MovieId");
                    je.HasData(
                        new { CharacterId = 1, MovieId = 1 },
                        new { CharacterId = 1, MovieId = 2 },
                        new { CharacterId = 2, MovieId = 1 },
                        new { CharacterId = 2, MovieId = 2 },
                        new { CharacterId = 3, MovieId = 3 }
                    );
                });
        }
    }
}