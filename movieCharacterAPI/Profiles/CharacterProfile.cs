﻿using movieCharacterAPI.Models.DTO.Character;
using movieCharacterAPI.Models.Domain;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace movieCharacterAPI.Profiles
{
    public class CharacterProfile : Profile
    {
        public CharacterProfile()
        {
            // Character <-> CharacterReadDTO
            CreateMap<Character, CharacterReadDTO>()
                // Turning related movies into int arrays
                .ForMember(adto => adto.Movies, opt => opt
                .MapFrom(c => c.Movies.Select(c => c.Id).ToArray()))
                .ReverseMap();

            // CharacterCreateDTO <-> Character
            CreateMap<CharacterCreateDTO, Character>().ReverseMap();

            // CharacterEditDTO <-> Character
            CreateMap<CharacterEditDTO, Character>().ReverseMap();
        }                                                   
    }
}
