﻿using movieCharacterAPI.Models.Domain;
using movieCharacterAPI.Models.DTO.Movie;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace movieCharacterAPI.Profiles
{
    public class MovieProfile : Profile
    {
        public MovieProfile()
        {
            // Movie <-> MovieReadDTO
            CreateMap<Movie, MovieReadDTO>()
                .ForMember(mdto => mdto.Franchise, opt => opt
                .MapFrom(m => m.FranchiseId))
                // Turning related characters into int arrays
                .ForMember(mdto => mdto.Characters, opt => opt
                .MapFrom(c => c.Characters.Select(c => c.Id).ToArray()))
                .ReverseMap();

            // MovieCreateDTO <-> Movie
            CreateMap<MovieCreateDTO, Movie>().ReverseMap();

            // MovieEditDTO <-> Movie
            CreateMap<MovieEditDTO, Movie>().ReverseMap();
        }
    }
}
