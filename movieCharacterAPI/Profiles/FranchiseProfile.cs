﻿using AutoMapper;
using movieCharacterAPI.Models.Domain;
using movieCharacterAPI.Models.DTO.Franchise;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace movieCharacterAPI.Profiles
{
    public class FranchiseProfile : Profile
    {
        public FranchiseProfile()
        {
            // Franchise <-> FranchiseReadDTO
            CreateMap<Franchise, FranchiseReadDTO>()
                // Turning related movies into int arrays
                .ForMember(adto => adto.Movies, opt => opt
                .MapFrom(c => c.Movies.Select(c => c.Id).ToArray()))
                .ReverseMap();

            // FranchiseCreateDTO <-> Franchise
            CreateMap<FranchiseCreateDTO, Franchise>().ReverseMap();

            // FranchiseEditDTO <-> Franchise
            CreateMap<FranchiseEditDTO, Franchise>().ReverseMap();
        }
    }
}
