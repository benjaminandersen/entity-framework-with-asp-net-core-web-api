﻿using Microsoft.EntityFrameworkCore;
using movieCharacterAPI.Models.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace movieCharacterAPI.Services
{
    public class MovieService : IMovieService
    {
        private readonly MovieCharacterDbContext _context;
        public MovieService(MovieCharacterDbContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<Movie>> GetAllMoviesAsync()
        {
            return await _context.Movies
                .Include(m => m.Characters)
                .ToListAsync();
        }

        public async Task<Movie> GetSpecificMovieAsync(int id)
        {
            return await _context.Movies
                .Include(m => m.Characters)
                .Where(m => m.Id == id)
                .FirstAsync();
        }

        public async Task<IEnumerable<Character>> GetAllCharactersInMovieAsync(int id)
        {
            return await _context.Movies
                .Where(m => m.Id == id)
                .SelectMany(m => m.Characters)
                .Include(c => c.Movies)
                .ToListAsync();
        }

        public async Task<Movie> AddMovieAsync(Movie movie)
        {
            _context.Movies.Add(movie);
            await _context.SaveChangesAsync();
            return movie;
        }

        public async Task UpdateMovieAsync(Movie movie)
        {
            _context.Entry(movie).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        public async Task UpdateMovieCharactersAsync(int id, List<int> characters)
        {
            Movie movieToUpdateCharacters = await _context.Movies
                .Include(m => m.Characters)
                .Where(m => m.Id == id)
                .FirstAsync();

            // Loop through characters, and assign to movie if found
            List<Character> actors = new();
            foreach (int characterId in characters)
            {
                Character character = await _context.Characters.FindAsync(characterId);
                if (character == null)
                {
                    throw new KeyNotFoundException();
                }
                actors.Add(character);
            }
            movieToUpdateCharacters.Characters = actors;
            await _context.SaveChangesAsync();
        }

        public async Task DeleteMovieAsync(int id)
        {
            var movie = await _context.Movies.FindAsync(id);
            _context.Movies.Remove(movie);
            await _context.SaveChangesAsync();
        }

        public bool MovieExists(int id)
        {
            return _context.Movies.Any(e => e.Id == id);
        }
    }
}
