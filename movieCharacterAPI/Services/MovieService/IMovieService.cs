﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using movieCharacterAPI.Models.Domain;
using movieCharacterAPI.Models.DTO.Franchise;


namespace movieCharacterAPI.Services
{
    public interface IMovieService
    {
        public Task<IEnumerable<Movie>> GetAllMoviesAsync();
        public Task<Movie> GetSpecificMovieAsync(int id);
        public Task<IEnumerable<Character>> GetAllCharactersInMovieAsync(int id);
        public Task<Movie> AddMovieAsync(Movie movie);
        public Task UpdateMovieAsync(Movie movie);
        public Task UpdateMovieCharactersAsync(int id, List<int> characters);
        public Task DeleteMovieAsync(int id);
        public bool MovieExists(int id);
    }
}
