﻿using movieCharacterAPI.Models.Domain;
using movieCharacterAPI.Models.DTO.Character;
using movieCharacterAPI.Models.DTO.Movie;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace movieCharacterAPI.Services
{
    public interface IFranchiseService
    {
        public Task<IEnumerable<Franchise>> GetAllFranchisesAsync();
        public Task<Franchise> GetSpecificFranchiseAsync(int id);
        public Task<IEnumerable<Movie>> GetAllMoviesInFranchiseAsync(int id);
        public Task<Franchise> AddFranchiseAsync(Franchise franchise);
        public Task UpdateFranchiseAsync(Franchise franchise);
        public Task UpdateFranchiseMoviesAsync(int id, List<int> movies);
        public Task DeleteFranchiseAsync(int id);
        public bool FranchiseExists(int id);
    }
}
