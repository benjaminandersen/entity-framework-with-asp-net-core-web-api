﻿using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Mvc;
using RouteAttribute = Microsoft.AspNetCore.Mvc.RouteAttribute;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using movieCharacterAPI.Services;
using movieCharacterAPI.Models.DTO.Franchise;
using movieCharacterAPI.Models.Domain;
using movieCharacterAPI.Models.DTO.Movie;
using movieCharacterAPI.Models.DTO.Character;


namespace movieCharacterAPI.Controllers
{
    [Route("api/franchises")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class FranchisesController : ControllerBase
    {
        // Add automapper via dependency injection
        private readonly IMapper _mapper;

        // Services work with domain objects, but controllers are still responsible for mapping
        private readonly IFranchiseService _franchiseService;
        private readonly IMovieService _movieService;
        private readonly ICharacterService _characterService;

        public FranchisesController(IMapper mapper, IFranchiseService franchiseService, IMovieService movieService, ICharacterService characterService)
        {
            _mapper = mapper;
            _franchiseService = franchiseService;
            _movieService = movieService;
            _characterService = characterService;
        }

        /// <summary>
        /// Get all franchises
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<FranchiseReadDTO>>> GetFranchises()
        {
            return _mapper.Map<List<FranchiseReadDTO>>(await _franchiseService.GetAllFranchisesAsync());
        }

        /// <summary>
        /// Get a specific franchise by ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<FranchiseReadDTO>> GetFranchise(int id)
        {
            Franchise franchise = await _franchiseService.GetSpecificFranchiseAsync(id);

            // Throws Not Found response if not found
            if (franchise == null)
            {
                return NotFound();
            }

            return _mapper.Map<FranchiseReadDTO>(franchise);
        }

        /// <summary>
        /// Get a list of all the movies in a specific franchise by ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}/movies")]
        public async Task<ActionResult<IEnumerable<MovieReadDTO>>> GetMoviesInFranchise(int id)
        {
            return _mapper.Map<List<MovieReadDTO>>(await _franchiseService.GetAllMoviesInFranchiseAsync(id));
        }

        /// <summary>
        /// Get a list of all the characters involved in a specific franchise by ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}/characters")]
        public async Task<ActionResult<IEnumerable<CharacterReadDTO>>> GetCharactersInFranchise(int id)
        {
            // First call the GetAllMoviesInFranchiseAsync() to get all movies
            var moviesInFranchise = _mapper.Map<List<MovieReadDTO>>(await _franchiseService.GetAllMoviesInFranchiseAsync(id));

            List<CharacterReadDTO> charactersdto = new();
            List<Character> characters = new();

            // Loop through movies in given franchise
            foreach (var movie in moviesInFranchise)
            {
                var charactersInMovie = _mapper.Map<List<Character>>(await _movieService.GetAllCharactersInMovieAsync(movie.Id));

                // Loop through characters in franchise's movies
                foreach (Character character in charactersInMovie)
                {
                    // Check if character already exists in list
                    if (!characters.Contains(character))
                    {
                        characters.Add(character);
                        // Convert character object to a characterReadDTO
                        var characterdto = _mapper.Map<CharacterReadDTO>(await _characterService.GetSpecificCharacterAsync(character.Id));
                        charactersdto.Add(characterdto);
                    }
                }
            }
            return charactersdto;
        }

        /// <summary>
        /// Add a new franchise to the database
        /// </summary>
        /// <param name="franchise"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<Franchise>> PostFranchise(FranchiseCreateDTO franchise)
        {
            Franchise domainFranchise = _mapper.Map<Franchise>(franchise);
            domainFranchise = await _franchiseService.AddFranchiseAsync(domainFranchise);

            return CreatedAtAction("GetFranchise",
                new { id = domainFranchise.Id },
                _mapper.Map<FranchiseReadDTO>(domainFranchise));
        }

        /// <summary>
        /// Update a specific franchise by ID
        /// (must pass a full franchise object and ID in route)
        /// </summary>
        /// <param name="id"></param>
        /// <param name="franchise"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFranchise(int id, FranchiseEditDTO franchise)
        {
            // Throws Bad Request response if IDs do not match
            if (id != franchise.Id)
            {
                return BadRequest();
            }

            // Throws Not Found response if not found
            if (!_franchiseService.FranchiseExists(id))
            {
                return NotFound();
            }

            Franchise domainFranchise = _mapper.Map<Franchise>(franchise);
            await _franchiseService.UpdateFranchiseAsync(domainFranchise);

            return NoContent();
        }

        /// <summary>
        /// Update a franchise's movies by franchise ID
        /// </summary>
        /// <param name="id"></param>
        /// <param name="movies"></param>
        /// <returns></returns>
        [HttpPut("{id}/movies")]
        public async Task<IActionResult> UpdateFranchiseMovies(int id, List<int> movies)
        {
            // Throws Not Found response if not found
            if (!_franchiseService.FranchiseExists(id))
            {
                return NotFound();
            }

            try
            {
                await _franchiseService.UpdateFranchiseMoviesAsync(id, movies);
            }
            catch (KeyNotFoundException)
            {
                return BadRequest("Invalid movie");
            }

            return NoContent();
        }

        /// <summary>
        /// Delete a franchise by specific ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteFranchise(int id)
        {
            // Throws Not Found response if not found
            if (!_franchiseService.FranchiseExists(id))
            {
                return NotFound();
            }

            await _franchiseService.DeleteFranchiseAsync(id);
            return NoContent();
        }
    }
}
